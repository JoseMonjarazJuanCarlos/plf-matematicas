# Matemáticas

## Conjuntos, Aplicaciones y funciones (2002)

```plantuml
@startmindmap
<style>
mindmapDiagram {
  Linecolor #FF4300
}
</style>

*[#044E51] <font color=#FFFFFF> Conjuntos, Aplicaciones \n<font color=#FFFFFF>          y funciones

	*_ su
		*[#079CA1] <font color=#FFFFFF>Definición
			*_ no
				*[#8AE0DB] Existe una definición precisa.
					*_ se
						*[#FFFFFF] Conoce 
							*_ como
								*[#FFFFFF] Conjunto de elementos.
								*[#FFFFFF] Colección de elementos.
			*_ es
				*[#8AE0DB] Una idea intuitiva
					*_ es
						*[#FFFFFF] Asumida
							*_ por
								*[#FFFFFF] El observador.
				*[#8AE0DB] Un concepto primitivo.

	*_ la
		*[#079CA1] <font color=#FFFFFF>Teoría del conjunto
			*_ se encarga
				*[#8AE0DB] De estudiar
					*_ las
						*[#FFFFFF] Propiedades
						*[#FFFFFF] Relaciones
			*_ se
				*[#8AE0DB] Caracteriza
					*_ por ser
						*[#FFFFFF] Racional
						*[#FFFFFF] Abstracta 

			*_ la
				*[#8AE0DB] Inclusión de conjuntos
					*_ es cuando
						*[#FFFFFF] Un conjunto está incluido en otro
							*_ y
								*[#FFFFFF] todos los elementos del primero \npertenecen al segundo.
					*_ conocido como
						*[#FFFFFF] Relación de inclusión.

	*_ la
		*[#079CA1] <font color=#FFFFFF>Operaciones
			*_ ayudan a
				*[#8AE0DB] Armar conjuntos complejos 
					*_ sobre
						*[#8AE0DB] Conjuntos elementales.
			*_ son
				*[#8AE0DB] Intersección
					*_ permite
						*[#FFFFFF] Formar un conjunto
							*_ sólo 
								*[#FFFFFF] con los elementos comunes involucrados en la operación.
				*[#8AE0DB] Unión 
					*_ toma
						*[#FFFFFF] Elementos que pertenecen al menos a uno de ellos.
				*[#8AE0DB] Complementación 
					*_ permite
						*[#FFFFFF] Formar un conjunto
							*_ con 
								*[#FFFFFF] Todos los elementos del conjunto de referencia o universal.
				*[#8AE0DB] Diferencia
					*_ permite
						*[#FFFFFF] Formar un conjunto
							*_ con 
								*[#FFFFFF] Elementos que pertenecen al primero pero no al segundo.

	*_ sus 
		*[#079CA1] <font color=#FFFFFF>Tipos
			*_ son
				*[#8AE0DB] Universal 
					*_ es
						*[#FFFFFF] Un conjunto de referecia.
					*_ en el
						*[#FFFFFF] En el que ocurre todas las cosas de la teoría.
				*[#8AE0DB] Vacío 
					*_ es 
						*[#FFFFFF] Una necesidad lógica para el cierre.
						*[#FFFFFF] Un Conjunto que no tiene elementos.

	*_ se
		*[#079CA1] <font color=#FFFFFF> Representa
			*_ mediante
				*[#8AE0DB] diagramas de Venn
					*_ en
						*[#FFFFFF] sistemas
							*_ los cuales son
								*[#FFFFFF] Círculos.
								*[#FFFFFF] Óvalos.
								*[#FFFFFF] Líneas cerradas.
					*_ propuesto por
						*[#FFFFFF] John Venn
					*_ no
						*[#FFFFFF] Sirve como método de demostración.

	*_ la
		*[#079CA1] <font color=#FFFFFF> Cardinalidad 
			*_ es el
				*[#8AE0DB] Número de elementos que lo conforman.
			*_ sus propiedades
				*[#8AE0DB] Formula Unión
					*[#FFFFFF] Es igual a la cardinalidad del conjunto A menos la cardinalidad del conjunto B menos el cardinal de la intersección.
					*[#FFFFFF] Elementos comunes.
			*_ tiene la
				*[#8AE0DB] Acotación de cardinales.
					*_ formula
						*[#FFFFFF] Serie de relaciones.

	*_ la
		*[#079CA1] <font color=#FFFFFF> Transformación 
			*_ la
				*[#8AE0DB] Historia del cambio
					*_ espera
						*[#FFFFFF] El elemento se convierte en el segundo.
			*_ es
				*[#8AE0DB] Basado
					*_ en
						*[#FFFFFF] Procesos de cambio.
			*_ su
				*[#8AE0DB] Aplicación
					*_ es
						*[#FFFFFF] Una regla
							*_ que
								*[#FFFFFF] Convierte a un conjunto origen en un \núnico elemento de un conjunto final.
					*_ sus
						*[#FFFFFF] Tipos
							*_ son
								*[#FFFFFF] Imagen inversa
								*[#FFFFFF] Imagen directa
								*[#FFFFFF] Inyectiva
								*[#FFFFFF] Sobreyectiva
								*[#FFFFFF] Biyectiva
					*_ la
						*[#FFFFFF] Función particular
							*_ son
								*[#FFFFFF] En el sistema de coordenadas. 
								*[#FFFFFF] Una serie de puntos.
@endmindmap
```

Recurso utilizado

[Video - Temas 6 y 7: Conjuntos, Aplicaciones y funciones](https://canal.uned.es/video/5a6f1b77b1111f15098b4609)

## Funciones (2010)

```plantuml
@startmindmap
<style>
mindmapDiagram {
  Linecolor #FF4300
}
</style>

*[#044E51] <font color=#FFFFFF> Funciones

	*_ tiene
		*[#079CA1] <font color=#FFFFFF>Antecedentes
			*_ refleja que
				*[#8AE0DB] El hombre 
					*_ para 
						*[#FFFFFF] Intentar comprender su entorno pensó.
					*_ resuelve
						*[#FFFFFF] Problemas cotidianos
							*_ con
								*[#FFFFFF] Ayuda de las matemáticas.
					*_ se enfrenta
						*[#FFFFFF] Al cambio
							*_ depende
								*[#FFFFFF] De las circunstancias
									*_ busca
										*[#FFFFFF] Aplicar las matemáticas.
			*_ se encontró
				*[#8AE0DB] Herramientas
					*_ que 
						*[#FFFFFF] Ayudaron a resolver problemas
							*_ de
								*[#FFFFFF] Cálculo 
								*[#FFFFFF] Medidas
								*[#FFFFFF] Distancias

	*_ su
		*[#079CA1] <font color=#FFFFFF>Concepto
			*_ es una
				*[#8AE0DB] Aplicación especial
					*_ son
						*[#FFFFFF] Conjuntos de números
							*_ los conjuntos
								*[#FFFFFF] Se relacionan.
								*[#FFFFFF] Se ven el cambio entre ellos.

	*_ se representan
		*[#079CA1] <font color=#FFFFFF>Gráficamente
			*_ por medio
				*[#8AE0DB] Graficación cartesiana
					*_ en honor
						*[#FFFFFF] A René Descartes
					*_ es
						*[#FFFFFF] Un sistema de coordenadas bidimensional.
							*_ consta
								*[#FFFFFF] De dos ejes.
								*[#FFFFFF] De cuatro cuadrantes.
			*_ se puede
				*[#8AE0DB] Observar el cambio 
					*_ del 
						*[#FFFFFF] Primer conjunto con respecto al segundo conjunto.

	*_ tiene
		*[#079CA1] <font color=#FFFFFF>Características
			*_ existen
				*[#8AE0DB] Crecientes
					*_ aumenta
						*[#FFFFFF] Variable independiente
							*_ conocido como
								*[#FFFFFF] Valores del primer conjunto.
								*[#FFFFFF] Variables del primer conjunto.
						*[#FFFFFF] Sus imágenes.
				*[#8AE0DB] Decrecientes
					*_ en el
						*[#FFFFFF] Son más pequeños.
			*_ son
				*[#8AE0DB] Relativos
					*_ este
						*[#FFFFFF] Pertenece al comportamiento.
					*_ dentro de
						*[#FFFFFF] Un intervalo
							*_ tiene
								*[#8AE0DB] Máximo
								*[#8AE0DB] Mínimo
			*_ su
				*[#8AE0DB] Límite
					*_ se refiere
						*[#FFFFFF] A su comportamiento local.
						*[#FFFFFF] Al como se aproxima los valores
							*_ de una 
								*[#FFFFFF] Función al acercarse al punto de interés.
					*_ se consideran
						*[#FFFFFF] Valores de la variable x que están cerca del punto.
						*[#FFFFFF] Valores de f(x).
					*_ sus
						*[#FFFFFF] Imágenes están cerca de un determinado valor.
			*_ puede ser
				*[#8AE0DB] Continua
					*_ sin
						*[#FFFFFF] Saltos.
					*_ tiene
						*[#FFFFFF] Buen comportamiento.
					*_ se
						*[#FFFFFF] Puede trabajar mejor con ellas.
				*[#8AE0DB] Discontinua
					*_ carece
						*[#FFFFFF] Del límite.

	*_ la
		*[#079CA1] <font color=#FFFFFF>Derivada
			*_ a través  
				*[#8AE0DB] De una función simple
					*_ aproxima
						*[#FFFFFF] Una función complicada.
			*_ existe
				*[#8AE0DB] Funciones más sencillas.
					*_ como
						*[#FFFFFF] Lineales
							*_ son
								*[#FFFFFF] Líneas rectas
			*_ resuelve
				*[#FFFFFF] El problema de la aproximación de una función compleja.
@endmindmap
```

Recursos utilizados

[Video - Funciones](https://canal.uned.es/video/5a6f2d09b1111f21778b457f)

## La matemática del computador (2002)

```plantuml
@startmindmap
<style>
mindmapDiagram {
  Linecolor #FF4300
}
</style>

*[#044E51] <font color=#FFFFFF> La matemática \n<font color=#FFFFFF>del computador

	*_ las
		*[#079CA1] <font color=#FFFFFF>Matemáticas
			*_ son
				*[#8AE0DB] Consideradas
					*_ el
						*[#FFFFFF] Cimiento de los ordenadores.
			*_ proveen
				*[#8AE0DB] Fundamentos matemáticos
					*_ para
						*[#FFFFFF] El funcionamiento del ordenador.
			*_ resuelve el
				*[#8AE0DB] Problema 
					*_ de
						*[#FFFFFF] Limitaciones
							*_ de mostrar
								*[#FFFFFF] Físicamente las infinitas cifras de ciertos números decimales 
									*_ en
										*[#FFFFFF] Un número finito de posiciones.
							*_ ejemplo 
								*[#FFFFFF] La calculadora.
					*_ con
						*[#FFFFFF] Representaciones.

	*_ la
		*[#079CA1] <font color=#FFFFFF>Aritmética del computador
			*_ posee
				*[#8AE0DB] Características
					*_ son
						*[#FFFFFF] Tiene un número finito de posiciones.
						*[#FFFFFF] Presenta problemas prácticos 
							*_ es
								*[#FFFFFF] Difícil realizar estos cómputos
									*_ en una 
										*[#FFFFFF] Máquina mecánica.
						*[#FFFFFF] El número real pierde su sentido .
						*[#FFFFFF] Se trabaja con números finitos de decimales.
			*_ presenta
				*[#8AE0DB] Aspectos matemáticos
					*_ son
						*[#FFFFFF] Número aproximado
							*_ es
								*[#FFFFFF] Un error
									*_ cometido
										*[#FFFFFF] Al aproximar un número decimal con infinitas cifras.
						*[#FFFFFF] Dígitos significativos
							*_ proporcionan 
								*[#FFFFFF] Información sobre la magnitud del signo.
									*_ +
									*_ -
						*[#FFFFFF] Técnicas 
							*_ existen
								*[#8AE0DB] Truncamiento
									*_ es
										*[#FFFFFF] Despreciar un determinado número de cifras.
											*_ es
												*[#FFFFFF] Dependiente
													*_ del
														*[#FFFFFF] Tipo de cómputo.
														*[#FFFFFF] Tipo de aplicación
									*_ se
										*[#FFFFFF] Corta un determinado número de cifras a partir de una dada.
									*_ sirve para
										*[#FFFFFF] Representar con un número finito \nde posiciones decimales
											*_ un
												*[#FFFFFF] Número muy grande.
												*[#FFFFFF] De infinitas cifras decimales
								*[#8AE0DB] Redondeo
									*_ es
										*[#FFFFFF] Despreciar un determinado número de cifras.
										*[#FFFFFF] Retocar la última cifra no despreciada.
									*_ se
										*[#FFFFFF] Intenta que el error cometido sea menor.
										*[#FFFFFF] Obtienen valores más pequeños.

	*_ la
		*[#079CA1] <font color=#FFFFFF>Aritmética en punto flotante
			*_ son
				*[#8AE0DB] Números
					*_ pueden ser
						*[#FFFFFF] Grandes.
						*[#FFFFFF] Pequeños.
					*_ se representan
						*[#FFFFFF] Como el producto
							*_ en
								*[#FFFFFF] Pontencias de 10
									*_ se
										*[#FFFFFF] Eleva un determinado exponente
											*_ se obtiene
												*[#FFFFFF] Una idea de su magnitud.
											*_ son ideas de
												*[#FFFFFF] Forma exponencial normalizada.
												*[#FFFFFF] Notación científica.

	*_ cuenta
		*[#079CA1] <font color=#FFFFFF>Sistemas
			*_ existe
				*[#8AE0DB] Binario
					*_ funciona
						*[#FFFFFF] Similar a un interruptor
							*_ el
								*[#FFFFFF] 0
									*[#FFFFFF] No pasa corriente.
								*[#FFFFFF] 1
									*[#FFFFFF] Pasa corriente.
					*_ presenta 
						*[#FFFFFF] la adición binaria
							*_ en donde
								*[#FFFFFF] Se suma en base dos
									*_ se deben
										*[#FFFFFF] Adaptar los números
					*_ tiene
						*[#FFFFFF] Características
							*_ son
								*[#FFFFFF] Se enlaza con la lógica booleana.
								*[#FFFFFF] Existe dualidad.
								*[#FFFFFF] Usa dos valores 0-1.
									*_ se
										*[#FFFFFF] Utilizan estos valores por la electrónica.
								*[#FFFFFF] Es el más fácil de representar mediante pulsos eléctricos.
				*[#8AE0DB] Octal
					*_ es
						*[#FFFFFF] Sistema de númeración
							*_ de
								*[#FFFFFF] Base 8.
						*[#FFFFFF] LLamado posicional.
					*_ utiliza
						*[#FFFFFF] Símbolos
							*_ los cuales son
								*[#FFFFFF] 0, 1, 2, 3, 4, 5, 6, 7
				*[#8AE0DB] Hexadecimal
					*_ es
						*[#FFFFFF] Sistema de númeración
							*_ de
								*[#FFFFFF] Base 17.
					*_ utiliza
						*[#FFFFFF] Símbolos
							*_ los cuales son
								*[#FFFFFF] 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C, D, E, F.
@endmindmap
```

Recursos utilizados

[Video 1 - La matemática del computador. El examen final](https://canal.uned.es/video/5a6f1b76b1111f15098b45fa)

[Video - 2 Tema 12: Las matemáticas de los computadores. El examen final](https://canal.uned.es/video/5a6f1b81b1111f15098b4640)